import axios, { AxiosResponse } from 'axios';
import IItemModel from './models/item-model';

const url: string = 'https://localhost:44312/api/list-items';

export const asyncApiStorage = {
    async addItem(model: IItemModel): Promise<string> {
        const response: AxiosResponse<string> = await axios.post(url, { id: model.Id, title: model.Title, isDone: model.IsDone });
        return response.data;
    },
    async initItems(): Promise<IItemModel[]> {
        const response: AxiosResponse<IItemModel[]> = await axios.get(url);
        return response.data;
    },
    async updateItem(model: IItemModel): Promise<void> {
       return await axios.put(url, { id: model.Id, title: model.Title, isDone: model.IsDone }, { params: { id: model.Id } });
    },
    async deleteItem(model: IItemModel): Promise<void> {
        return await axios.delete(url, { params: { id: model.Id }});
    },
    async clearItems(): Promise<void> {
        return await axios.get(url + '/clear');
    },
  };
