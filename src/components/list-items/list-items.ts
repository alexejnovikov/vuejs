import Vue from 'vue';
import Component from 'vue-class-component';
import { Actions } from '../../enums/actions';
import { Getters } from '../../enums/getters';
import { ItemsFilter } from '../../enums/items-filter';
import IItemModel from '../../models/item-model';
import TodoItem from './../todo-item/todo-item.vue';

@Component({
  components: { TodoItem },
})
export default class ListItems extends Vue {
    public filterValue: ItemsFilter = ItemsFilter.All;
    private newItemText: string = '';
    public addNewItem(): void {
        if (this.newItemText) {
          const model: IItemModel = {
            Id: '',
            IsDone: false,
            Title: this.newItemText,
          };
          this.$store.dispatch(Actions.Create, model);
          this.newItemText = '';
        }
    }
    public deleteItem(item: IItemModel): void {
      this.$store.dispatch(Actions.Delete, item);
    }
    public clearItems(): void {
       const doneItems: IItemModel[] = this.$store.getters[Getters.getFilteredItems](ItemsFilter.Completed);
       this.$store.dispatch(Actions.ClearItems, doneItems);
    }
    public get filteredItems(): IItemModel[] {
      return this.$store.getters[Getters.getFilteredItems](this.filterValue);
    }
    public get total(): number {
      return this.$store.getters[Getters.getAllItems].length;
    }
    public get selected(): number {
      return (this.selectedCount / this.$store.getters[Getters.getAllItems].length) * 100;
    }
    public get leftItems(): number {
      return this.$store.getters[Getters.getAllItems].length - this.selectedCount;
    }
    public get selectedCount(): number {
      return this.$store.getters[Getters.getFilteredItems](ItemsFilter.Completed).length;
    }
}
