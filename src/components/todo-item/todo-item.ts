import Vue from 'vue';
import Component from 'vue-class-component';
import { Prop } from 'vue-property-decorator';
import { Actions } from '../../enums/actions';
import IItemModel from '../../models/item-model';

@Component({})
export default class TodoItem extends Vue {
    @Prop({ required: true }) public item!: IItemModel;

    public dialog = false;
    public originalItem: IItemModel = this.item;

    public editItem(): void {
      this.originalItem = Object.assign({}, this.item);
    }
    public markAsDone(): void {
      this.$store.dispatch(Actions.Update, this.item);
    }
    public saveItem(): void {
      if (this.originalItem.Title) {
        this.$store.dispatch(Actions.Update, this.originalItem);
        this.dialog = false;
      }
    }
    public cancelItem(): void {
      this.dialog = false;
    }
    public removeItem(): void {
      this.$emit('removeItem', this.item);
    }
}
