import axios from 'axios';
import 'material-design-icons-iconfont/dist/material-design-icons.css';
import Vue from 'vue';
import VueAxios from 'vue-axios';
import Vuetify from 'vuetify/lib';

Vue.use(Vuetify, VueAxios, axios);

export default new Vuetify({
  icons: {
    iconfont: 'md',
  },
  theme: {
    themes: {
      light: {
        primary: '#4DBA87',
        secondary: '#FFFFFF',
      },
    },
  },
});
