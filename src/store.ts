import Vue from 'vue';
import Vuex from 'vuex';
import { asyncApiStorage } from './async-api-storage';
import { Actions } from './enums/actions';
import { Getters } from './enums/getters';
import { ItemsFilter } from './enums/items-filter';
import { Mutations } from './enums/mutations';
import IItemModel from './models/item-model';

Vue.use(Vuex);

export default new Vuex.Store({
  actions: {
    [Actions.Create]: async (context, model: IItemModel): Promise<void> => {
        model.Id = await asyncApiStorage.addItem(model);
        context.commit(Mutations.addItem, model);
    },
    [Actions.Delete]: async (context, model: IItemModel): Promise<void> => {
        await asyncApiStorage.deleteItem(model);
        context.commit(Mutations.deleteItem, model.Id);
    },
    [Actions.Update]: async (context, model: IItemModel): Promise<void> => {
        await asyncApiStorage.updateItem(model);
        context.commit(Mutations.updateItem, { model} );
    },
    [Actions.InitStore]: async (context) => {
        const data: IItemModel[]  = await asyncApiStorage.initItems();
        context.commit(Mutations.initStore, data);
    },
    [Actions.ClearItems]: async (context, model: IItemModel[]) => {
        await asyncApiStorage.clearItems();
        model.forEach((item: IItemModel) => context.commit(Mutations.deleteItem, item.Id));
    },
  },
  getters: {
    [Getters.getAllItems]: (state): IItemModel[] => {
        return state.listItems;
    },
    [Getters.getFilteredItems]: (state) => (filterValue: ItemsFilter): IItemModel[] => {
        switch (filterValue) {
          case ItemsFilter.All:
              return state.listItems;
          case ItemsFilter.Active:
              return state.listItems.filter((f) => !f.IsDone);
          case ItemsFilter.Completed:
              return state.listItems.filter((f) => f.IsDone);
        }
        return state.listItems;
    },
  },
  mutations: {
    [Mutations.deleteItem]: (state, id: string): void => {
      const index = state.listItems.findIndex((f) => f.Id === id);
      if (index > -1) {
        state.listItems.splice(index, 1);
      }
    },
    [Mutations.initStore]: (state, data): void => {
      state.listItems = data;
    },
    [Mutations.addItem]: (state, model: IItemModel): void => {
      state.listItems.push(model);
    },
    [Mutations.updateItem]: (state, { model } ): void => {
      const index = state.listItems.findIndex((f) => f.Id === model.Id);
      if (index > -1) {
        state.listItems[index].Title = model.Title;
        state.listItems[index].IsDone = model.IsDone;
      }
    },
  },
  state: {
    listItems: [
      {
        Id: '',
        IsDone: true,
        Title: '',
      },
    ],
  },
});
