import Vue from 'vue';
import App from './App.vue';
import { Actions } from './enums/actions';
import vuetify from './plugins/vuetify';
import store from './store';

Vue.config.productionTip = false;

new Vue({
  render: (h) => h(App),
  store,
  vuetify,
  beforeCreate() {
    this.$store.dispatch(Actions.InitStore);
  },
}).$mount('#app');
