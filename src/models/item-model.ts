export default interface IItemModel {
    Id: string;
    Title: string;
    IsDone: boolean;
}
