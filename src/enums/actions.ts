export enum Actions {
    Create = 'addTodoItem',
    Delete = 'removeTodoItem',
    Update = 'updateTodoItem',
    InitStore = 'initStore',
    ClearItems = 'clearItems',
}
