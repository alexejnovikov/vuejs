export enum Mutations {
    deleteItem = 'deleteItem',
    initStore = 'initStore',
    addItem = 'addItem',
    updateItem = 'updateItem',
}
