export enum Getters {
    getAllItems = 'getAllItems',
    getFilteredItems = 'getFilteredItems',
}
